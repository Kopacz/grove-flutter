import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:grove_flutter/app/grove_app.dart';

import 'data/di/service_locator.dart';

void main() {
  initServiceLocator();

  overlaySystemBars();

  runApp(const GroveApp());
}

void overlaySystemBars() {
  SystemChrome.setEnabledSystemUIMode(
    SystemUiMode.edgeToEdge,
    overlays: [SystemUiOverlay.top, SystemUiOverlay.bottom],
  );

  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle.light.copyWith(systemNavigationBarColor: Colors.transparent),
  );
}


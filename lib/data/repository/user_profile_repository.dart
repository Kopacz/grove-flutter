import 'package:grove_flutter/data/storage/device_storage.dart';

class UserProfileRepository {
  static const _userProfileKey = 'user_profile';

  final DeviceStorage storage;

  UserProfileRepository({required this.storage});

  Future<String?> getUserProfile() async {
    return await storage.readString(_userProfileKey);
  }

  Future<void> saveUserProfile(String userProfile) async {
    await storage.writeString(_userProfileKey, userProfile);
  }

  Future<void> clearUserProfile() async {
    await storage.removeString(_userProfileKey);
  }
}

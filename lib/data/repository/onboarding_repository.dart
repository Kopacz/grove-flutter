import 'package:grove_flutter/data/storage/device_storage.dart';

class OnboardingRepository {
  static const _onboardingKey = 'onboarding';

  final DeviceStorage storage;

  OnboardingRepository({required this.storage});

  Future<bool?> isOnboardingDisplayed() async {
    return await storage.readBoolean(_onboardingKey);
  }

  Future<void> setOnboardingDisplayed(bool displayed) async {
    await storage.writeBoolean(_onboardingKey, displayed);
  }
}

import 'package:get_it/get_it.dart';
import 'package:grove_flutter/app/app_cubit.dart';
import 'package:grove_flutter/data/repository/onboarding_repository.dart';
import 'package:grove_flutter/data/repository/user_profile_repository.dart';
import 'package:grove_flutter/data/storage/device_storage.dart';
import 'package:grove_flutter/feature/authentication/cubit/login_cubit.dart';
import 'package:grove_flutter/feature/dashboard/main/dashboard_cubit.dart';
import 'package:grove_flutter/feature/dashboard/profile/cubit/profile_cubit.dart';
import 'package:grove_flutter/feature/onboarding/cubit/onboarding_cubit.dart';

final GetIt serviceLocator = GetIt.instance;

void initServiceLocator() {
  _setupStorage();
  _setupRepositories();
  _setupCubits();
}

void _setupStorage() {
  serviceLocator.registerFactory(() => DeviceStorage());
}

void _setupRepositories() {
  serviceLocator.registerFactory(() => OnboardingRepository(storage: serviceLocator.get<DeviceStorage>()));
  serviceLocator.registerFactory(() => UserProfileRepository(storage: serviceLocator.get<DeviceStorage>()));
}

void _setupCubits() {
  serviceLocator.registerFactory(() => AppCubit(repository: serviceLocator.get<OnboardingRepository>()));
  serviceLocator.registerFactory(() => OnboardingCubit(repository: serviceLocator.get<OnboardingRepository>()));
  serviceLocator.registerFactory(() => DashboardCubit());
  serviceLocator.registerFactory(() => LoginCubit(repository: serviceLocator.get<UserProfileRepository>()));
  serviceLocator.registerFactory(() => ProfileCubit(repository: serviceLocator.get<UserProfileRepository>()));
}

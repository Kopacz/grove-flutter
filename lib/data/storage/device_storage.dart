import 'package:shared_preferences/shared_preferences.dart';

class DeviceStorage {

  Future<bool?> readBoolean(String key) async {
    final preferences = await SharedPreferences.getInstance();
    return preferences.getBool(key);
  }

  Future<void> writeBoolean(String key, bool data) async {
    final preferences = await SharedPreferences.getInstance();
    await preferences.setBool(key, data);
  }

  Future<void> removeBoolean(String key) async {
    final preferences = await SharedPreferences.getInstance();
    await preferences.remove(key);
  }

  Future<String?> readString(String key) async {
    final preferences = await SharedPreferences.getInstance();
    return preferences.getString(key);
  }

  Future<void> writeString(String key, String data) async {
    final preferences = await SharedPreferences.getInstance();
    await preferences.setString(key, data);
  }

  Future<void> removeString(String key) async {
    final preferences = await SharedPreferences.getInstance();
    await preferences.remove(key);
  }
}

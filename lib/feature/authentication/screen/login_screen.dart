import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_signin_button/flutter_signin_button.dart';
import 'package:grove_flutter/app/app_router.dart';
import 'package:grove_flutter/data/di/service_locator.dart';
import 'package:grove_flutter/feature/authentication/cubit/login_cubit.dart';
import 'package:grove_flutter/feature/authentication/cubit/login_state.dart';
import 'package:grove_flutter/resources/assets.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/loader/loader.dart';
import 'package:grove_flutter/ui/spacer/margin.dart';
import 'package:grove_flutter/ui/text/app_typography.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => serviceLocator.get<LoginCubit>(),
      child: const Scaffold(
        body: _LoginBody(),
      ),
    );
  }
}

class _LoginBody extends StatelessWidget {
  const _LoginBody();

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginCubit, LoginState>(
      listener: (context, state) {
        state.when(
          idle: () {},
          loading: () {},
          success: () => _navigateUp(context),
          error: (_) => _navigateUp(context),
        );
      },
      child: BlocBuilder<LoginCubit, LoginState>(
        builder: (context, state) {
          return Stack(
            children: [
              const _BackgroundImage(Assets.concertImage),
              const _BackgroundImage(Assets.logoTriangleImage),
              const _Shadow(),
              Padding(
                padding: const EdgeInsets.all(Dimensions.marginNormal),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    const VerticalMargin(Dimensions.marginLarge),
                    _BackButton(() => _pop(context)),
                    const VerticalMargin(Dimensions.marginNormal),
                    const _Logo(),
                    const VerticalMargin(Dimensions.marginNormal),
                    const _Title(),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          const _Description(),
                          const VerticalMargin(Dimensions.marginNormal),
                          SignInButton(
                            Buttons.Google,
                            text: Strings.authenticationLoginButtonText,
                            padding: const EdgeInsets.symmetric(
                              vertical: Dimensions.marginSmall,
                            ),
                            onPressed: () => _login(context),
                          ),
                          const VerticalMargin(Dimensions.marginNormal),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              if (state == const LoginState.loading()) ...[
                const Center(child: Loader())
              ],
            ],
          );
        },
      ),
    );
  }

  void _pop(BuildContext context) {
    Navigator.of(context).pop();
  }

  void _navigateUp(BuildContext context) {
    Navigator.of(context).pushNamedAndRemoveUntil(
      AppRouter.dashboard,
      (_) => false,
    );
  }

  void _login(BuildContext context) {
    context.read<LoginCubit>().login();
  }
}

class _BackgroundImage extends StatelessWidget {
  final String imageAsset;

  const _BackgroundImage(this.imageAsset);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(imageAsset),
          opacity: 1,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class _Shadow extends StatelessWidget {
  const _Shadow();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Palette.black.withOpacity(0.3),
    );
  }
}

class _BackButton extends StatelessWidget {
  final Function() onClick;

  const _BackButton(this.onClick);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: GestureDetector(
        onTap: onClick,
        child: Row(
          children: [
            Icon(
              Icons.arrow_back,
              color: Palette.white,
              size: Dimensions.iconSizeDefault,
            ),
            Text(
              Strings.backLabel,
              style: getTextTheme(context).bodyLarge,
            )
          ],
        ),
      ),
    );
  }
}

class _Logo extends StatelessWidget {
  const _Logo();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: Dimensions.marginNormal,
      ),
      child: Image.asset(
        Assets.logoTextImage,
        fit: BoxFit.cover,
      ),
    );
  }
}

class _Title extends StatelessWidget {
  const _Title({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      Strings.authenticationLoginTitle,
      textAlign: TextAlign.center,
      style: getTextTheme(context).displayMedium,
    );
  }
}

class _Description extends StatelessWidget {
  const _Description();

  @override
  Widget build(BuildContext context) {
    return Text(
      Strings.authenticationLoginDescription,
      style: getTextTheme(context).titleMedium,
      textAlign: TextAlign.center,
    );
  }
}

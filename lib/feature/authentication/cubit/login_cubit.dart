import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grove_flutter/data/repository/user_profile_repository.dart';
import 'package:grove_flutter/feature/authentication/cubit/login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  final UserProfileRepository repository;

  LoginCubit({required this.repository}) : super(const LoginState.idle());

  Future<void> login() async {
    emit(const LoginState.loading());

    await Future<dynamic>.delayed(const Duration(seconds: 3));

    try {
      await repository.saveUserProfile("DummyUserId");
      emit(const LoginState.success());
    } catch (exception) {
      emit(const LoginState.error(errorMessage: 'Failed to login.'));
    }
  }
}

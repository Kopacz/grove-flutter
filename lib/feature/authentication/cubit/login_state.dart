import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_state.freezed.dart';

@freezed
class LoginState with _$LoginState {
  const factory LoginState.idle() = Idle;

  const factory LoginState.loading() = Loading;

  const factory LoginState.success() = Success;

  const factory LoginState.error({required String errorMessage}) = Error;
}



import 'package:freezed_annotation/freezed_annotation.dart';

part 'onboarding_state.freezed.dart';

@freezed
class OnboardingState with _$OnboardingState {
  const factory OnboardingState.idle() = Idle;

  const factory OnboardingState.loading() = Loading;

  const factory OnboardingState.success({
    required bool onboardingStateSaved,
  }) = Success;

  const factory OnboardingState.error({required String errorMessage}) = Error;
}

import 'package:flutter/material.dart';
import 'package:grove_flutter/resources/assets.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/button/circle_button.dart';
import 'package:grove_flutter/ui/button/gradient_button.dart';
import 'package:grove_flutter/ui/button/skip_button.dart';
import 'package:grove_flutter/ui/dialog/rounded_corner_bottom_sheet.dart';
import 'package:grove_flutter/ui/spacer/margin.dart';
import 'package:grove_flutter/ui/text/app_typography.dart';
import 'package:lottie/lottie.dart';

void showLocationDialog({
  required BuildContext context,
  required Function() onClose,
  required Function() onSkip,
  required Function() onLocationEnabled,
}) {
  showRoundedCornerBottomSheet(
    context: context,
    child: FractionallySizedBox(
      heightFactor: 0.7,
      child: Stack(
        children: [
          const _BackgroundImage(),
          const _LocationAnimation(),
          Positioned(
            top: 0.0,
            right: 0.0,
            child: PlainCircleButton(icon: Icons.close, onPressed: onClose),
          ),
          Positioned(
            bottom: 0.0,
            left: 0.0,
            right: 0.0,
            child: Padding(
              padding: const EdgeInsets.all(Dimensions.marginLarge),
              child: Column(
                children: [
                  const _Title(),
                  const VerticalMargin(Dimensions.marginSemiLarge),
                  const _Description(),
                  const VerticalMargin(Dimensions.marginExtraLarge),
                  _EnableLocationButton(onClick: onLocationEnabled),
                  const VerticalMargin(Dimensions.marginNormal),
                  _SkipButton(onClick: onSkip),
                  const VerticalMargin(Dimensions.marginBottomSheetBottom),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

class _BackgroundImage extends StatelessWidget {
  const _BackgroundImage();

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(Dimensions.roundedCornerNormal),
        topRight: Radius.circular(Dimensions.roundedCornerNormal),
      ),
      child: Image.asset(Assets.mapImage),
    );
  }
}

class _LocationAnimation extends StatelessWidget {
  const _LocationAnimation();

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Lottie.asset(
        Assets.onboardingLocationAnimation,
        width: 138.0,
        height: 138.0,
      ),
    );
  }
}

class _Title extends StatelessWidget {
  const _Title();

  @override
  Widget build(BuildContext context) {
    return Text(
      Strings.onboardingLocationTitle,
      style: getTextTheme(context).titleMedium,
      textAlign: TextAlign.center,
    );
  }
}

class _Description extends StatelessWidget {
  const _Description();

  @override
  Widget build(BuildContext context) {
    return Text(
      Strings.onboardingLocationDescription,
      style: getTextTheme(context).bodyMedium,
      textAlign: TextAlign.center,
    );
  }
}

class _EnableLocationButton extends StatelessWidget {
  final Function() onClick;

  const _EnableLocationButton({required this.onClick});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GradientButton(
          label: Strings.onboardingLocationButtonText,
          onPressed: onClick,
        ),
      ],
    );
  }
}

class _SkipButton extends StatelessWidget {
  final Function() onClick;

  const _SkipButton({required this.onClick});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SkipButton(onClick),
      ],
    );
  }
}

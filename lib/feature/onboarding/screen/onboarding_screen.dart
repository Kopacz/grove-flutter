import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grove_flutter/app/app_router.dart';
import 'package:grove_flutter/data/di/service_locator.dart';
import 'package:grove_flutter/feature/onboarding/cubit/onboarding_cubit.dart';
import 'package:grove_flutter/feature/onboarding/screen/onboarding_location_dialog.dart';
import 'package:grove_flutter/feature/onboarding/screen/onboarding_page.dart';
import 'package:grove_flutter/feature/onboarding/state/onboarding_state.dart';
import 'package:grove_flutter/resources/assets.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/loader/loader.dart';
import 'package:lottie/lottie.dart';

class OnboardingScreen extends StatefulWidget {
  const OnboardingScreen();

  @override
  State<OnboardingScreen> createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  final PageController controller = PageController();
  int currentPageNumber = 0;

  static const Duration pageAnimationDuration = Duration(milliseconds: 300);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => serviceLocator.get<OnboardingCubit>(),
      child: Scaffold(
        body: BlocConsumer<OnboardingCubit, OnboardingState>(
          listener: (context, state) {
            state.when(
              idle: () => null,
              loading: () => null,
              success: (_) => _openDashboard(),
              error: (_) => _openDashboard(),
            );
          },
          builder: (context, state) {
            return Container(
              color: Palette.backgroundDarkColor,
              child: Stack(
                children: [
                  Column(
                    children: [
                      Flexible(
                        flex: 3,
                        child:
                            _Animation(_getAnimationForPage(currentPageNumber)),
                      ),
                      Flexible(
                        flex: 2,
                        child: _Pager(
                          controller: controller,
                          onPageChanged: _updatePage,
                          onNextPage: _openNextPage,
                          onSkip: () =>
                              _setOnboardingDisplayed(context: context),
                          onDone: () {
                            _openLocationDialog(
                              onClose: _closeLocationDialog,
                              onSkip: () => {
                                _closeLocationDialog(),
                                _setOnboardingDisplayed(context: context),
                              },
                              onLocationEnabled: () => {
                                _closeLocationDialog(),
                                _setOnboardingDisplayed(context: context),
                              },
                            );
                          },
                        ),
                      )
                    ],
                  ),
                  if (state == const OnboardingState.loading()) ...[
                    const Center(
                      child: Loader(),
                    )
                  ],
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  String _getAnimationForPage(int pageNumber) {
    switch (pageNumber) {
      case 0:
        return Assets.onboardingRoomsAnimation;
      case 1:
        return Assets.onboardingCommunitiesAnimation;
      case 2:
        return Assets.onboardingExploreAnimation;
      default:
        return Assets.onboardingRoomsAnimation;
    }
  }

  void _updatePage(int pageNumber) {
    setState(() {
      currentPageNumber = pageNumber;
    });
  }

  void _openNextPage() {
    controller.nextPage(
      duration: pageAnimationDuration,
      curve: Curves.easeIn,
    );

    setState(() {
      currentPageNumber += 1;
    });
  }

  void _openDashboard() {
    Navigator.of(context).pushNamedAndRemoveUntil(
      AppRouter.dashboard,
      (_) => false,
    );
  }

  void _openLocationDialog({
    required Function() onClose,
    required Function() onSkip,
    required Function() onLocationEnabled,
  }) {
    showLocationDialog(
      context: context,
      onClose: onClose,
      onSkip: onSkip,
      onLocationEnabled: onLocationEnabled,
    );
  }

  void _closeLocationDialog() {
    Navigator.of(context).pop();
  }

  void _setOnboardingDisplayed({required BuildContext context}) {
    context.read<OnboardingCubit>().setOnboardingDisplayed();
  }
}

class _Animation extends StatelessWidget {
  final String animation;

  const _Animation(this.animation);

  @override
  Widget build(BuildContext context) {
    return Lottie.asset(animation);
  }
}

class _Pager extends StatelessWidget {
  final PageController controller;
  final void Function(int) onPageChanged;
  final Function() onNextPage;
  final Function() onSkip;
  final Function() onDone;

  const _Pager({
    required this.controller,
    required this.onPageChanged,
    required this.onNextPage,
    required this.onSkip,
    required this.onDone,
  });

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: controller,
      onPageChanged: onPageChanged,
      children: [
        OnboardingPage(
          title: Strings.onboardingPage1Title,
          description: Strings.onboardingPage1Description,
          indicatorAsset: Assets.indicatorDots1Vector,
          onNext: onNextPage,
          onSkip: onSkip,
        ),
        OnboardingPage(
          title: Strings.onboardingPage2Title,
          description: Strings.onboardingPage2Description,
          indicatorAsset: Assets.indicatorDots2Vector,
          onNext: onNextPage,
          onSkip: onSkip,
        ),
        OnboardingPage(
          title: Strings.onboardingPage3Title,
          description: Strings.onboardingPage3Description,
          indicatorAsset: Assets.indicatorDots3Vector,
          onNext: onDone,
          onSkip: onSkip,
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/button/gradient_button.dart';
import 'package:grove_flutter/ui/button/skip_button.dart';
import 'package:grove_flutter/ui/spacer/margin.dart';
import 'package:grove_flutter/ui/text/app_typography.dart';

class OnboardingPage extends StatelessWidget {
  final String title;
  final String description;
  final String indicatorAsset;
  final Function() onNext;
  final Function() onSkip;

  const OnboardingPage({
    required this.title,
    required this.description,
    required this.indicatorAsset,
    required this.onNext,
    required this.onSkip,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Palette.backgroundColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Divider(
            height: Dimensions.dividerSizeDefault,
            thickness: Dimensions.dividerSizeDefault,
            color: Palette.contentColor,
          ),
          Padding(
            padding: const EdgeInsets.all(Dimensions.marginLarge),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                _Title(title),
                const VerticalMargin(Dimensions.marginSmall),
                _Description(description),
                const VerticalMargin(Dimensions.marginNormal),
                _PageActions(
                  indicatorAsset: indicatorAsset,
                  onSkip: onSkip,
                  onNext: onNext,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class _Title extends StatelessWidget {
  final String text;

  const _Title(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: getTextTheme(context).titleMedium,
    );
  }
}

class _Description extends StatelessWidget {
  final String text;

  const _Description(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: getTextTheme(context).bodyMedium,
    );
  }
}

class _PageActions extends StatelessWidget {
  final String indicatorAsset;
  final Function() onSkip;
  final Function() onNext;

  const _PageActions({
    required this.indicatorAsset,
    required this.onSkip,
    required this.onNext,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _Indicator(indicatorAsset),
            const VerticalMargin(Dimensions.marginNormal),
            SkipButton(onSkip)
          ],
        ),
        GradientButton(
          label: Strings.nextLabel,
          icon: Icons.arrow_forward,
          onPressed: onNext,
        ),
      ],
    );
  }
}

class _Indicator extends StatelessWidget {
  final String asset;

  const _Indicator(this.asset);

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(asset);
  }
}

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grove_flutter/data/repository/onboarding_repository.dart';
import 'package:grove_flutter/feature/onboarding/state/onboarding_state.dart';

class OnboardingCubit extends Cubit<OnboardingState> {
  final OnboardingRepository repository;

  OnboardingCubit({required this.repository})
      : super(const OnboardingState.idle());

  Future<void> setOnboardingDisplayed() async {
    emit(const OnboardingState.loading());

    try {
      await repository.setOnboardingDisplayed(true);
      emit(const OnboardingState.success(onboardingStateSaved: true));
    } catch (exception) {
      emit(OnboardingState.error(errorMessage: exception.toString()));
    }
  }
}

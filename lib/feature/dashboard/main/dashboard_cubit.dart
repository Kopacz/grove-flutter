import 'package:flutter_bloc/flutter_bloc.dart';

import 'dashboard_state.dart';

class DashboardCubit extends Cubit<DashboardState> {
  DashboardCubit() : super(const DashboardState.loading());

  Future<void> loadDashboardData() async {
    await Future<dynamic>.delayed(const Duration(seconds: 3));

    emit(const DashboardState.success());
  }

  void doNothing() {

  }
}

import 'package:freezed_annotation/freezed_annotation.dart';

part 'dashboard_state.freezed.dart';

@freezed
class DashboardState with _$DashboardState {
  const factory DashboardState.loading() = Loading;

  const factory DashboardState.success() = Success;

  const factory DashboardState.error({required String errorMessage}) = Error;
}

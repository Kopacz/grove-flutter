import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grove_flutter/data/di/service_locator.dart';
import 'package:grove_flutter/feature/dashboard/main/dashboard_cubit.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/profile_screen.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/text/grey_text.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  State<DashboardScreen> createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  int _selectedTab = 1;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<DashboardCubit>(
      create: (_) => serviceLocator.get<DashboardCubit>()..loadDashboardData(),
      child: Scaffold(
        body: _tabs.elementAt(_selectedTab),
        bottomNavigationBar: _BottomNavigation(
          selectedTab: _selectedTab,
          onTabItemClicked: _onTabItemClicked,
        ),
      ),
    );
  }

  void _onTabItemClicked(int index) {
    setState(() {
      _selectedTab = index;
    });
  }

  static const List<Widget> _tabs = <Widget>[
    _TabPlaceholder(text: 'Tab 0: Home'),
    _TabPlaceholder(text: 'Tab 1: Explore'),
    _TabPlaceholder(text: 'Tab 2: Post'),
    _TabPlaceholder(text: 'Tab 3: Activity'),
    ProfileTab(),
  ];
}

class _BottomNavigation extends StatelessWidget {
  final int selectedTab;
  final Function(int) onTabItemClicked;

  const _BottomNavigation({
    required this.selectedTab,
    required this.onTabItemClicked,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Palette.backgroundColor,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          const Divider(
            height: Dimensions.dividerSizeDefault,
            thickness: Dimensions.dividerSizeDefault,
          ),
          BottomNavigationBar(
            items: _tabItems,
            currentIndex: selectedTab,
            onTap: onTabItemClicked,
            type: BottomNavigationBarType.fixed,
          ),
          Padding(
            padding:
                EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
          )
        ],
      ),
    );
  }

  static const List<BottomNavigationBarItem> _tabItems = [
    BottomNavigationBarItem(
      icon: Icon(Icons.home),
      label: Strings.dashboardHomeItemLabel,
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.explore),
      label: Strings.dashboardExploreItemLabel,
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.video_call),
      label: Strings.dashboardPostItemLabel,
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.add_alert_sharp),
      label: Strings.dashboardActivityItemLabel,
    ),
    BottomNavigationBarItem(
      icon: Icon(Icons.account_circle_outlined),
      label: Strings.dashboardProfileItemLabel,
    ),
  ];
}

class _TabPlaceholder extends StatelessWidget {
  final String text;

  const _TabPlaceholder({required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColorDark,
      child: Center(
        child: GreyText(text),
      ),
    );
  }
}

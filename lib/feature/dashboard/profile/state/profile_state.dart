import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:grove_flutter/domain/model/profile_data.dart';

part 'profile_state.freezed.dart';

@freezed
class ProfileState with _$ProfileState {
  const factory ProfileState.loading() = Loading;

  const factory ProfileState.success({
    required ProfileData profileData,
  }) = Success;

  const factory ProfileState.error({required String errorMessage}) = Error;
}

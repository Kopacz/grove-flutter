import 'package:flutter/material.dart';
import 'package:grove_flutter/domain/model/like_interaction.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/common/profile_tab_empty_content.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/common/profile_tab_scaffold.dart';
import 'package:grove_flutter/resources/assets.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/scroll/scroll_listener.dart';

import 'like_tile.dart';

class LikesTab extends StatefulWidget {
  final List<LikeInteraction> likes;
  final ScrollListener scrollListener;

  const LikesTab({
    required this.likes,
    required this.scrollListener,
  });

  @override
  State<LikesTab> createState() => _LikesTabState();
}

class _LikesTabState extends State<LikesTab> {
  final ScrollController _scrollController = ScrollController();
  double _previousScroll = 0;

  @override
  void initState() {
    _scrollController.addListener(() {
      final maxScroll = _scrollController.position.maxScrollExtent;

      setState(() {
        _scrollController.handleScrollEvents(
          previousScroll: _previousScroll,
          maxScroll: maxScroll,
          listener: widget.scrollListener,
        );
        _previousScroll = _scrollController.offset;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ProfileTabScaffold(
      label: Strings.profileLikesTabLabel,
      child: MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: widget.likes.isNotEmpty ? ListView(
          primary: false,
          controller: _scrollController,
          children: _getItems(widget.likes),
        ) : TabEmptyContent(
          imageAssetName: Assets.friendsImage,
          icon: Icons.favorite,
          title: Strings.profileLikesInitialTitleText,
          description: Strings.profileLikesInitialDescriptionText,
          buttonText: Strings.profileLikesInitialButtonText,
          onButtonClick: () => {null},
        ),
      ),
    );
  }

  List<Widget> _getItems(List<LikeInteraction> likes) {
    return List.generate(
      likes.length,
      (index) => LikeTile(like: likes[index]),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:grove_flutter/domain/model/like_interaction.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/spacer/margin.dart';
import 'package:grove_flutter/ui/text/app_typography.dart';
import 'package:grove_flutter/ui/text/grey_text.dart';

class LikeTile extends StatelessWidget {
  final LikeInteraction like;

  const LikeTile({required this.like});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: Dimensions.marginNormal,
        vertical: Dimensions.marginSmall,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _Avatar(like.avatar),
          const HorizontalMargin(Dimensions.marginMedium),
          Expanded(
            child: _LikeInfo(
              alias: like.alias,
              clipName: like.clipName,
            ),
          ),
          const HorizontalMargin(Dimensions.marginMedium),
          GreyText(like.date),
        ],
      ),
    );
  }
}

class _Avatar extends StatelessWidget {
  final String avatar;

  const _Avatar(this.avatar);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: Palette.dividerColor,
      radius: Dimensions.avatarRadiusDefault + Dimensions.borderDefault,
      child: CircleAvatar(
        backgroundImage: NetworkImage(avatar),
        radius: Dimensions.avatarRadiusDefault,
      ),
    );
  }
}

class _LikeInfo extends StatelessWidget {
  final String alias;
  final String clipName;

  const _LikeInfo({
    required this.alias,
    required this.clipName,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              alias,
              style: TextStyle(
                fontStyle: FontStyle.italic,
                fontSize: 14,
                color: Palette.contentColor,
              ),
            ),
            Expanded(
              child: Text(
                Strings.profileLikesInteractionText,
                style: TextStyle(
                  fontStyle: FontStyle.normal,
                  fontSize: 14,
                  color: Palette.white,
                  fontWeight: FontWeight.bold,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
        const VerticalMargin(Dimensions.marginExtraSmall),
        Text(
          clipName,
          style:  getTextTheme(context).bodyLarge,
        )
      ],
    );
  }
}

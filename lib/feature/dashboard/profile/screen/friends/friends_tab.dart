import 'package:flutter/material.dart';
import 'package:grove_flutter/domain/model/relationship.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/common/profile_tab_empty_content.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/friends/friend_tile.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/common/profile_tab_scaffold.dart';
import 'package:grove_flutter/resources/assets.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/scroll/scroll_listener.dart';

class FriendsTab extends StatefulWidget {
  final List<Relationship> friends;
  final ScrollListener scrollListener;

  const FriendsTab({
    required this.friends,
    required this.scrollListener,
  });

  @override
  State<FriendsTab> createState() => _FriendsTabState();
}

class _FriendsTabState extends State<FriendsTab> {
  final ScrollController _scrollController = ScrollController();
  double _previousScroll = 0;

  @override
  void initState() {
    _scrollController.addListener(() {
      final maxScroll = _scrollController.position.maxScrollExtent;

      setState(() {
        _scrollController.handleScrollEvents(
          previousScroll: _previousScroll,
          maxScroll: maxScroll,
          listener: widget.scrollListener,
        );
        _previousScroll = _scrollController.offset;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ProfileTabScaffold(
      label: Strings.profileFriendsTabLabel,
      child: MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: widget.friends.isNotEmpty ? ListView(
          primary: false,
          controller: _scrollController,
          children: _getItems(widget.friends),
        ) : TabEmptyContent(
          imageAssetName: Assets.friendsImage,
          icon: Icons.people_alt_outlined,
          title: Strings.profileFriendsInitialTitleText,
          description: Strings.profileFriendsInitialDescriptionText,
          buttonText: Strings.profileFriendsInitialButtonText,
          onButtonClick: () => {null},
        ),
      ),
    );
  }

  List<Widget> _getItems(List<Relationship> friends) {
    return List.generate(
      friends.length,
      (index) => FriendTile(friend: friends[index]),
    );
  }
}

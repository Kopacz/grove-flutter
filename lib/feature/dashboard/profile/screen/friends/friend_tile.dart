import 'package:flutter/material.dart';
import 'package:grove_flutter/domain/model/relationship.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/ui/spacer/margin.dart';
import 'package:grove_flutter/ui/text/app_typography.dart';
import 'package:grove_flutter/ui/text/grey_text.dart';

class FriendTile extends StatelessWidget {
  final Relationship friend;

  const FriendTile({required this.friend});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: Dimensions.marginNormal,
        vertical: Dimensions.marginSmall,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _FriendAvatar(friend.avatar),
          const HorizontalMargin(Dimensions.marginMedium),
          Expanded(child: _FriendData(friend.name, friend.alias)),
          const HorizontalMargin(Dimensions.marginMedium),
          _RelationshipStatusLabel(_getLabelForStatus(friend.status)),
        ],
      ),
    );
  }

  String _getLabelForStatus(RelationshipStatus status) {
    switch (status) {
      case RelationshipStatus.accepted:
        return "Friend";
      case RelationshipStatus.invited:
        return "Pending";
      case RelationshipStatus.declined:
        return "Declined";
    }
  }
}

class _FriendAvatar extends StatelessWidget {
  final String avatar;

  const _FriendAvatar(this.avatar);

  @override
  Widget build(BuildContext context) {
    return CircleAvatar(
      backgroundColor: Palette.dividerColor,
      radius: Dimensions.avatarRadiusDefault + Dimensions.borderDefault,
      child: CircleAvatar(
        backgroundImage: NetworkImage(avatar),
        radius: Dimensions.avatarRadiusDefault,
      ),
    );
  }
}

class _FriendData extends StatelessWidget {
  final String name;
  final String alias;

  const _FriendData(this.name, this.alias);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          name,
          style:  getTextTheme(context).titleSmall,
        ),
        const VerticalMargin(Dimensions.marginExtraSmall),
        Text(
          alias,
          style: TextStyle(
            fontStyle: FontStyle.italic,
            fontSize: 14,
            color: Palette.contentColor,
          ),
        )
      ],
    );
  }
}

class _RelationshipStatusLabel extends StatelessWidget {
  final String status;

  const _RelationshipStatusLabel(this.status);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          color: Palette.contentColor,
          width: Dimensions.borderSmall,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(Dimensions.roundedCornerNormal),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: Dimensions.marginLarge,
          vertical: Dimensions.marginMedium,
        ),
        child: GreyText(status),
      ),
    );
  }
}

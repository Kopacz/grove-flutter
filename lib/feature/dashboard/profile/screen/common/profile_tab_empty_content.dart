import 'package:flutter/material.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/ui/button/gradient_button.dart';
import 'package:grove_flutter/ui/spacer/margin.dart';
import 'package:grove_flutter/ui/text/app_typography.dart';

class TabEmptyContent extends StatelessWidget {
  final String imageAssetName;
  final IconData icon;
  final String title;
  final String description;
  final String buttonText;
  final Function() onButtonClick;

  const TabEmptyContent({
    required this.imageAssetName,
    required this.icon,
    required this.title,
    required this.description,
    required this.buttonText,
    required this.onButtonClick,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: Dimensions.marginLarge,
          horizontal: Dimensions.marginSmall,
        ),
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(imageAssetName),
              fit: BoxFit.fitWidth,
            ),
          ),
          child: Column(
            children: [
              Icon(
                icon,
                color: Palette.white,
                size: Dimensions.iconSizeLarge,
              ),
              const VerticalMargin(Dimensions.marginNormal),
              Text(
                title,
                style: getTextTheme(context).titleLarge,
                textAlign: TextAlign.center,
              ),
              const VerticalMargin(Dimensions.marginSemiLarge),
              Text(
                description,
                style: getTextTheme(context).bodyLarge,
                textAlign: TextAlign.center,
              ),
              const VerticalMargin(Dimensions.marginSemiLarge),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GradientButton(
                    label: buttonText,
                    onPressed: onButtonClick,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

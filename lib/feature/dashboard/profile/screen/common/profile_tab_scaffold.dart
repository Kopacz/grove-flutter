import 'package:flutter/material.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/ui/labels/header_view.dart';

class ProfileTabScaffold extends StatelessWidget {
  final String label;
  final Widget child;

  const ProfileTabScaffold({
    required this.label,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        HeaderView(text: label),
        const Divider(
          height: Dimensions.dividerSizeDefault,
          thickness: Dimensions.dividerSizeDefault,
        ),
        Expanded(
          child: Container(
            color: Palette.backgroundDarkColor,
            child: child,
          ),
        ),
      ],
    );
  }
}

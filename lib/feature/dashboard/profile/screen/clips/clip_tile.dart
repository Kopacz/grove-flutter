import 'package:flutter/material.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/domain/model/clip.dart';
import 'package:grove_flutter/ui/icon/optional_icon.dart';

class ClipTile extends StatelessWidget {
  final Clip data;

  const ClipTile({required this.data});

  @override
  Widget build(BuildContext context) {
    return _ClipTileBackground(
      cover: data.cover,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          OptionalIcon(
            isVisible: data.type == ClipType.video,
            icon: Icons.play_arrow,
          ),
          OptionalIcon(
            isVisible: data.isLiked,
            icon: Icons.favorite,
            size: Dimensions.iconSizeMedium,
          ),
        ],
      ),
    );
  }
}

class _ClipTileBackground extends StatelessWidget {
  final String cover;
  final Widget child;

  const _ClipTileBackground({
    required this.cover,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(cover),
          fit: BoxFit.cover,
        ),
      ),
      padding: const EdgeInsets.all(Dimensions.marginSmall),
      child: Align(
        alignment: Alignment.topCenter,
        child: child,
      ),
    );
  }
}

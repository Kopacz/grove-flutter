import 'package:flutter/material.dart';
import 'package:grove_flutter/domain/model/clip.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/clips/clip_tile.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/common/profile_tab_empty_content.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/common/profile_tab_scaffold.dart';
import 'package:grove_flutter/resources/assets.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/scroll/scroll_listener.dart';

class ClipsTab extends StatefulWidget {
  final List<Clip> clips;
  final ScrollListener scrollListener;

  const ClipsTab({
    required this.clips,
    required this.scrollListener,
  });

  @override
  State<ClipsTab> createState() => _ClipsTabState();
}

class _ClipsTabState extends State<ClipsTab> {
  final ScrollController _scrollController = ScrollController();
  double _previousScroll = 0;

  @override
  void initState() {
    _scrollController.addListener(() {
      final maxScroll = _scrollController.position.maxScrollExtent;

      setState(() {
        _scrollController.handleScrollEvents(
          previousScroll: _previousScroll,
          maxScroll: maxScroll,
          listener: widget.scrollListener,
        );
        _previousScroll = _scrollController.offset;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ProfileTabScaffold(
      label: Strings.profileClipsTabLabel,
      child: widget.clips.isNotEmpty
          ? GridView.count(
              primary: false,
              crossAxisCount: 3,
              padding: const EdgeInsets.all(Dimensions.dividerSizeDefault),
              mainAxisSpacing: Dimensions.dividerSizeDefault,
              crossAxisSpacing: Dimensions.dividerSizeDefault,
              controller: _scrollController,
              children: _getTiles(widget.clips),
            )
          : TabEmptyContent(
              imageAssetName: Assets.clipsImage,
              icon: Icons.video_call_outlined,
              title: Strings.profileClipsInitialTitleText,
              description: Strings.profileClipsInitialDescriptionText,
              buttonText: Strings.profileClipsInitialButtonText,
              onButtonClick: () => {null},
            ),
    );
  }

  List<Widget> _getTiles(List<Clip> clips) {
    return List.generate(
      clips.length,
      (index) => ClipTile(
        data: clips[index],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grove_flutter/app/app_router.dart';
import 'package:grove_flutter/data/di/service_locator.dart';
import 'package:grove_flutter/domain/model/profile_data.dart';
import 'package:grove_flutter/feature/dashboard/profile/cubit/profile_cubit.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/clips/clips_tab.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/friends/friends_tab.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/likes/likes_tab.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/rooms/rooms_tab.dart';
import 'package:grove_flutter/feature/dashboard/profile/state/profile_state.dart';
import 'package:grove_flutter/resources/assets.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/button/circle_button.dart';
import 'package:grove_flutter/ui/button/gradient_button.dart';
import 'package:grove_flutter/ui/dialog/options_dialog/options_bottom_sheet.dart';
import 'package:grove_flutter/ui/dialog/options_dialog/option_horizontal.dart';
import 'package:grove_flutter/ui/loader/loader.dart';
import 'package:grove_flutter/ui/profile/profile_view.dart';
import 'package:grove_flutter/ui/profile/profile_view_spec.dart';
import 'package:grove_flutter/ui/scroll/scroll_listener.dart';
import 'package:grove_flutter/ui/text/app_typography.dart';
import 'package:grove_flutter/ui/text/grey_text.dart';
import 'package:tabbar_gradient_indicator/tabbar_gradient_indicator.dart';

class ProfileTab extends StatelessWidget {
  const ProfileTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProfileCubit>(
      create: (_) => serviceLocator.get<ProfileCubit>()..loadUserProfile(),
      child: const _ProfileBody(),
    );
  }
}

class _ProfileBody extends StatefulWidget {
  const _ProfileBody({Key? key}) : super(key: key);

  @override
  State<_ProfileBody> createState() => _ProfileBodyState();
}

class _ProfileBodyState extends State<_ProfileBody> {
  late ScrollListener scrollListener;
  var collapsed = false;

  @override
  void initState() {
    scrollListener = ScrollListener(
      onScrolledTop: () => setState(() {
        collapsed = false;
      }),
      onScrolledDown: () => setState(() {
        collapsed = true;
      }),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double paddingTop = ProfileViewSpec.paddingTop;
    final double collapsedHeight = ProfileViewSpec.collapsedHeight;
    final double expandedHeight = ProfileViewSpec.expandedHeight;
    final double gap = ProfileViewSpec.normalGap;
    final double tabBarHeight = 86.0;

    return BlocBuilder<ProfileCubit, ProfileState>(
      builder: (cubitContext, state) {
        return Container(
          color: Palette.backgroundDarkColor,
          child: state.when(
            loading: () => const Center(
              child: Loader(),
            ),
            success: (profile) => Stack(
              fit: StackFit.expand,
              children: [
                Positioned(
                  top: 0.0,
                  height: collapsed
                      ? paddingTop + collapsedHeight + tabBarHeight
                      : paddingTop + expandedHeight + tabBarHeight,
                  child: ProfileView(
                    profile: profile,
                    coverAsset: Assets.defaultCover,
                    collapsed: collapsed,
                  ),
                ),
                Positioned(
                  top: collapsed
                      ? paddingTop + collapsedHeight + gap
                      : paddingTop + expandedHeight + gap,
                  bottom: MediaQuery.of(context).viewInsets.bottom,
                  child: ProfilePages(
                    profile: profile,
                    scrollListener: scrollListener,
                  ),
                ),
                profile.mode == ProfileMode.user
                    ? Positioned(
                        top: MediaQuery.of(context).viewPadding.top,
                        right: 0.0,
                        child: PlainCircleButton(
                          icon: Icons.more_horiz,
                          onPressed: () {
                            showOptionsBottomSheet(
                              context: context,
                              children: _getBottomSheetItems(),
                            );
                          },
                        ),
                      )
                    : Positioned(
                        top: MediaQuery.of(context).viewPadding.top,
                        right: Dimensions.marginMedium,
                        child: GradientButton(
                          label: Strings.loginLabel,
                          icon: Icons.arrow_forward,
                          onPressed: _openLoginScreen,
                        ),
                      )
              ],
            ),
            error: (error) => Center(
              child: GreyText(error),
            ),
          ),
        );
      },
    );
  }

  List<OptionHorizontal> _getBottomSheetItems() {
    return [
      OptionHorizontal(
        label: Strings.logoutLabel,
        icon: Icons.account_circle_outlined,
        onClick: () {
          context.read<ProfileCubit>().logout();
        },
      )
    ];
  }

  void _openLoginScreen() {
    Navigator.of(context).pushNamed(AppRouter.login);
  }
}

class ProfilePages extends StatelessWidget {
  final ProfileData profile;
  final ScrollListener scrollListener;

  const ProfilePages({
    required this.profile,
    required this.scrollListener,
  });

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Container(
        color: Colors.transparent,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            TabBar(
              indicator: TabBarGradientIndicator(
                indicatorWidth: Dimensions.indicatorSizeDefault,
                gradientColor: [Palette.fuegoGreen, Palette.javaBlue],
              ),
              indicatorPadding: const EdgeInsets.only(
                left: Dimensions.marginSmall,
                right: Dimensions.marginSmall,
              ),
              tabs: [
                ProfileTabItem(
                  counter: profile.rooms.length.toString(),
                  text: Strings.profileRoomsTabItemLabel,
                ),
                ProfileTabItem(
                  counter: profile.clips.length.toString(),
                  text: Strings.profileClipsTabItemLabel,
                ),
                ProfileTabItem(
                  counter: profile.likes.length.toString(),
                  text: Strings.profileLikesTabItemLabel,
                ),
                ProfileTabItem(
                  counter: profile.friends.length.toString(),
                  text: Strings.profileFriendsTabItemLabel,
                ),
              ],
            ),
            Expanded(
              child: TabBarView(
                children: [
                  RoomsTab(
                    rooms: profile.rooms,
                    scrollListener: scrollListener,
                  ),
                  ClipsTab(
                    clips: profile.clips,
                    scrollListener: scrollListener,
                  ),
                  LikesTab(
                    likes: profile.likes,
                    scrollListener: scrollListener,
                  ),
                  FriendsTab(
                    friends: profile.friends,
                    scrollListener: scrollListener,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ProfileTabItem extends StatelessWidget {
  final String counter;
  final String text;

  const ProfileTabItem({
    required this.counter,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: Dimensions.marginSmall),
      child: Column(
        children: [
          Text(
            counter,
            style:  getTextTheme(context).displayLarge,
            textAlign: TextAlign.center,
            maxLines: 1,
          ),
          Text(
            text,
            style:  getTextTheme(context).bodyMedium,
            textAlign: TextAlign.center,
            maxLines: 1,
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:grove_flutter/domain/model/room.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/ui/icon/optional_icon.dart';
import 'package:grove_flutter/ui/spacer/margin.dart';
import 'package:grove_flutter/ui/text/app_typography.dart';

class RoomTile extends StatelessWidget {
  final Room room;

  const RoomTile({required this.room});

  @override
  Widget build(BuildContext context) {
    return _RoomTileBackground(
      cover: room.cover,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          _RoomTitle(room.title),
          _RoomTileBottomIcons(room),
        ],
      ),
    );
  }
}

class _RoomTileBackground extends StatelessWidget {
  final String cover;
  final Widget child;

  const _RoomTileBackground({
    required this.cover,
    required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(cover),
          fit: BoxFit.cover,
        ),
        borderRadius: const BorderRadius.all(
          Radius.circular(Dimensions.roundedCornerSmall),
        ),
      ),
      padding: const EdgeInsets.fromLTRB(
        Dimensions.marginSmall,
        Dimensions.marginSmall,
        Dimensions.marginExtraSmall,
        Dimensions.marginExtraSmall,
      ),
      child: child,
    );
  }
}

class _RoomTitle extends StatelessWidget {
  final String title;

  const _RoomTitle(this.title);

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style:  getTextTheme(context).titleSmall,
    );
  }
}

class _RoomTileBottomIcons extends StatelessWidget {
  final Room room;

  const _RoomTileBottomIcons(this.room);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        const _BottomIcon(Icons.people),
        _BottomIconLabel(room.members),
        const HorizontalMargin(Dimensions.marginExtraSmall),
        const _BottomIcon(Icons.favorite),
        _BottomIconLabel(room.likes),
        const HorizontalMargin(Dimensions.marginExtraSmall),
        const _BottomIcon(Icons.movie),
        _BottomIconLabel(room.views),
        const HorizontalMargin(Dimensions.marginExtraSmall),
        Expanded(
          child: Align(
            alignment: Alignment.centerRight,
            child: OptionalIcon(
              isVisible: room.isMembered,
              icon: Icons.account_circle_outlined,
              size: Dimensions.iconSizeSemiMedium,
            ),
          ),
        ),
      ],
    );
  }
}

class _BottomIcon extends StatelessWidget {
  final IconData iconData;

  const _BottomIcon(this.iconData);

  @override
  Widget build(BuildContext context) {
    return Icon(
      iconData,
      color: Palette.white,
      size: Dimensions.iconSizeSmall,
    );
  }
}

class _BottomIconLabel extends StatelessWidget {
  final String text;

  const _BottomIconLabel(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style:  getTextTheme(context).bodySmall,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:grove_flutter/domain/model/room.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/common/profile_tab_empty_content.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/common/profile_tab_scaffold.dart';
import 'package:grove_flutter/feature/dashboard/profile/screen/rooms/room_tile.dart';
import 'package:grove_flutter/resources/assets.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/scroll/scroll_listener.dart';

class RoomsTab extends StatefulWidget {
  final List<Room> rooms;
  final ScrollListener scrollListener;

  const RoomsTab({
    required this.rooms,
    required this.scrollListener,
  });

  @override
  State<RoomsTab> createState() => _RoomsTabState();
}

class _RoomsTabState extends State<RoomsTab> {
  final ScrollController _scrollController = ScrollController();
  double _previousScroll = 0;

  @override
  void initState() {
    _scrollController.addListener(() {
      final maxScroll = _scrollController.position.maxScrollExtent;

      setState(() {
        _scrollController.handleScrollEvents(
          previousScroll: _previousScroll,
          maxScroll: maxScroll,
          listener: widget.scrollListener,
        );
        _previousScroll = _scrollController.offset;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ProfileTabScaffold(
      label: Strings.profileRoomsTabLabel,
      child: widget.rooms.isNotEmpty
          ? GridView.count(
              primary: false,
              crossAxisCount: 2,
              padding: const EdgeInsets.all(Dimensions.marginNormal),
              mainAxisSpacing: Dimensions.marginNormal,
              crossAxisSpacing: Dimensions.marginNormal,
              controller: _scrollController,
              children: _getTiles(widget.rooms),
            )
          : TabEmptyContent(
              imageAssetName: Assets.roomsImage,
              icon: Icons.explore,
              title: Strings.profileRoomsInitialTitleText,
              description: Strings.profileRoomsInitialDescriptionText,
              buttonText: Strings.profileRoomsInitialButtonText,
              onButtonClick: () => {null},
            ),
    );
  }

  List<Widget> _getTiles(List<Room> rooms) {
    return List.generate(
      rooms.length,
      (index) => RoomTile(room: rooms[index]),
    );
  }
}

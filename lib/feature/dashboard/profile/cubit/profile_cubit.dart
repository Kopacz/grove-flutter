import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grove_flutter/data/repository/user_profile_repository.dart';
import 'package:grove_flutter/domain/model/profile_data.dart';
import 'package:grove_flutter/feature/dashboard/profile/cubit/mock_profile_data.dart';
import 'package:grove_flutter/feature/dashboard/profile/state/profile_state.dart';
import 'package:grove_flutter/resources/assets.dart';

class ProfileCubit extends Cubit<ProfileState> {
  final UserProfileRepository repository;

  ProfileCubit({required this.repository})
      : super(const ProfileState.loading());

  Future<void> loadUserProfile() async {
    emit(const ProfileState.loading());

    await _fetchProfile(
      onProfileExists: (profile) {
        emit(
          ProfileState.success(profileData: MockProfileData.mockProfileData),
        );
      },
      onProfileNotExist: () {
        emit(
          ProfileState.success(profileData: _createGuestProfile()),
        );
      },
    );
  }

  Future<void> logout() async {
    emit(const ProfileState.loading());

    await repository.clearUserProfile();
    await _fetchProfile(
      onProfileExists: (profile) {
        emit(
          const ProfileState.error(
            errorMessage: "Failed to login. Please, try again.",
          ),
        );
      },
      onProfileNotExist: () {
        emit(
          ProfileState.success(profileData: _createGuestProfile()),
        );
      },
    );
  }

  Future<void> _fetchProfile({
    required void Function(String) onProfileExists,
    required void Function() onProfileNotExist,
  }) async {
    await Future<dynamic>.delayed(const Duration(seconds: 1));
    final userProfile = await repository.getUserProfile();

    if (userProfile == null) {
      onProfileNotExist();
    } else {
      onProfileExists(userProfile);
    }
  }

  ProfileData _createGuestProfile() {
    return ProfileData(
      mode: ProfileMode.guest,
      name: "Guest",
      surname: "",
      alias: "",
      description: "",
      avatarUrl: Assets.avatarGuestPlaceholder,
      rooms: List.empty(),
      clips: List.empty(),
      likes: List.empty(),
      friends: List.empty(),
    );
  }
}

import 'package:grove_flutter/domain/model/clip.dart';
import 'package:grove_flutter/domain/model/like_interaction.dart';
import 'package:grove_flutter/domain/model/profile_data.dart';
import 'package:grove_flutter/domain/model/relationship.dart';
import 'package:grove_flutter/domain/model/room.dart';

class MockProfileData {
  MockProfileData._();

  static final mockRooms = List.generate(
    20,
    (index) => Room(
      cover: 'https://picsum.photos/200',
      title: 'Lorem ipsum $index',
      members: '25',
      likes: '225',
      views: '9k+',
      isMembered: true,
    ),
  );

  static final mockClips = List.generate(
    40,
    (index) => Clip(
      type: ClipType.video,
      cover: 'https://picsum.photos/200',
      isLiked: true,
    ),
  );

  static final mockLikes = List.generate(
    20,
    (index) => LikeInteraction(
      avatar: "https://placekitten.com/250/250",
      alias: "@elisabeth",
      clipName: "Clip Name $index",
      date: "Jul 15, 2021",
    ),
  );

  static final mockFriends = List.generate(
    30,
    (index) => Relationship(
      avatar: "https://placekitten.com/250/250",
      name: "John Doe",
      alias: "@johndoe$index",
      status: RelationshipStatus.accepted,
    ),
  );

  static final mockProfileData = ProfileData(
    mode: ProfileMode.user,
    name: 'Tomasz',
    surname: 'Kopacz',
    alias: '@tkop',
    description: 'Hello, it\'s me! Invite me to your room!',
    avatarUrl: 'https://www.woolha.com/media/2020/03/eevee.png',
    rooms: mockRooms,
    clips: mockClips,
    likes: mockLikes,
    friends: mockFriends,
  );
}

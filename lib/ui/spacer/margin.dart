import 'package:flutter/cupertino.dart';
import 'package:grove_flutter/resources/palette.dart';

class VerticalMargin extends StatelessWidget {
  final double? height;

  const VerticalMargin(this.height);

  @override
  Widget build(BuildContext context) {
    return SizedBox(height: height);
  }
}

class HorizontalMargin extends StatelessWidget {
  final double? width;

  const HorizontalMargin(this.width);

  @override
  Widget build(BuildContext context) {
    return SizedBox(width: width);
  }
}

import 'package:flutter/material.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/text/grey_text.dart';

class SkipButton extends StatelessWidget {
  final Function() onClick;

  const SkipButton(this.onClick);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClick,
      child: Row(
        children: [
          const GreyText(Strings.skipLabel),
          Icon(
            Icons.keyboard_arrow_right,
            color: Palette.manateeGrey,
            size: Dimensions.iconSizeSemiMedium,
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';

class PlainCircleButton extends StatelessWidget {
  final IconData icon;
  final Function() onPressed;

  const PlainCircleButton({
    required this.icon,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        shape: const CircleBorder(),
        padding: const EdgeInsets.all(
          Dimensions.marginExtraSmall,
        ),
        fixedSize: const Size(
          Dimensions.iconSizeMedium,
          Dimensions.iconSizeMedium,
        ),
        backgroundColor: Palette.white,
      ),
      child: Icon(
        icon,
        color: Palette.black,
        size: Dimensions.iconSizeDefault,
      ),
    );
  }
}

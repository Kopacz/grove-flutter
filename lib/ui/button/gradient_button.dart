import 'package:flutter/material.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/ui/spacer/margin.dart';

class GradientButton extends StatelessWidget {
  final String label;
  final IconData? icon;
  final Function() onPressed;

  const GradientButton({
    required this.label,
    this.icon,
    required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: Dimensions.marginSmall),
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Palette.fuegoGreen,
              Palette.javaBlue,
            ],
          ),
          borderRadius: const BorderRadius.all(
            Radius.circular(
              Dimensions.roundedCornerNormal,
            ),
          ),
        ),
        child: OutlinedButton(
          onPressed: onPressed,
          style: OutlinedButton.styleFrom(
            padding: const EdgeInsets.all(Dimensions.marginNormal),
            side: BorderSide(
              color: Palette.white,
              width: Dimensions.borderSmall,
            ),
            backgroundColor: Colors.transparent,
            shadowColor: Colors.transparent,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(
                  Dimensions.roundedCornerNormal,
                ),
              ),
            ),
          ),
          child: Row(
            children: [
              Text(
                label,
                style: TextStyle(
                  color: Palette.white,
                  fontSize: 14,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.bold,
                ),
              ),
              if (icon != null) ...[
                const HorizontalMargin(Dimensions.marginExtraSmall),
                Icon(
                  icon,
                  color: Palette.white,
                  size: Dimensions.iconSizeSmall,
                ),
              ],
            ],
          ),
        ),
      ),
    );
  }
}

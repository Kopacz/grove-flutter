import 'package:flutter/material.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/ui/dialog/rounded_corner_bottom_sheet.dart';
import 'package:grove_flutter/ui/spacer/margin.dart';

void showOptionsBottomSheet({
  required BuildContext context,
  required List<Widget> children,
}) {
  showRoundedCornerBottomSheet(
    context: context,
    child: Padding(
      padding: const EdgeInsets.only(
        left: Dimensions.marginNormal,
        right: Dimensions.marginNormal,
        top: Dimensions.marginSmall,
        bottom: Dimensions.marginBottomSheetBottom,
      ),
      child: Wrap(
        direction: Axis.vertical,
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: Dimensions.marginNormal,
            alignment: Alignment.topCenter,
            child: SizedBox(
              width: 48.0,
              child: Divider(
                thickness: Dimensions.borderDefault,
                color: Palette.contentColor,
              ),
            ),
          ),
          ...children,
        ],
      ),
    ),
  );
}

import 'package:flutter/material.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';
import 'package:grove_flutter/ui/spacer/margin.dart';
import 'package:grove_flutter/ui/text/app_typography.dart';

class OptionHorizontal extends StatelessWidget {
  final String label;
  final IconData icon;
  final Function() onClick;

  const OptionHorizontal({
    required this.label,
    required this.icon,
    required this.onClick,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
        onClick();
      },
      child: Row(
        children: [
          CircleAvatar(
            backgroundColor: Palette.contentColor,
            radius: Dimensions.avatarRadiusSmall,
            child: Icon(
              icon,
              color: Palette.white,
              size: Dimensions.iconSizeSemiMedium,
            ),
          ),
          const HorizontalMargin(Dimensions.marginNormal),
          Text(
            label,
            style:  getTextTheme(context).titleSmall,
          )
        ],
      ),
    );
  }
}

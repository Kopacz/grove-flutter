import 'package:flutter/material.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';

void showRoundedCornerBottomSheet({
  required BuildContext context,
  required Widget child,
}) {
  showModalBottomSheet<void>(
    context: context,
    isScrollControlled: true,
    backgroundColor: Palette.backgroundColor,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(
          Dimensions.roundedCornerNormal,
        ),
      ),
    ),
    builder: (BuildContext context) {
      return child;
    },
  );
}

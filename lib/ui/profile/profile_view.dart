import 'package:flutter/material.dart';
import 'package:grove_flutter/domain/model/profile_data.dart';
import 'package:grove_flutter/ui/profile/profile_view_spec.dart';
import 'package:grove_flutter/ui/profile/profile_view_widgets.dart';
import 'package:grove_flutter/ui/text/app_typography.dart';

class ProfileView extends StatelessWidget {
  final ProfileData profile;
  final String coverAsset;
  final bool collapsed;

  const ProfileView({
    required this.profile,
    required this.coverAsset,
    required this.collapsed,
  });

  static const animationDurationMs = 200;
  final duration = const Duration(milliseconds: animationDurationMs);

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(coverAsset),
          fit: BoxFit.cover,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: ProfileViewSpec.paddingTop),
        child: AnimatedSize(
          duration: duration,
          child: SizedBox(
            height: collapsed ? ProfileViewSpec.collapsedHeight : ProfileViewSpec.expandedHeight,
            width: screenWidth,
            child: Stack(
              children: _getComponents(
                profile: profile,
                collapsed: collapsed,
                duration: duration,
                screenWidth: screenWidth,
                context: context,
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _getComponents({
    required ProfileData profile,
    required bool collapsed,
    required Duration duration,
    required double screenWidth,
    required BuildContext context,
  }) {
    final components = <Widget>[];

    final avatar = ProfileAnimatedAvatar(
      avatarUrl: profile.avatarUrl,
      radius: collapsed ? ProfileViewSpec.avatarSmallRadius : ProfileViewSpec.avatarLargeRadius,
      top: collapsed ? ProfileViewSpec.avatarCollapsedTop : ProfileViewSpec.avatarExpandedTop,
      left: collapsed ? ProfileViewSpec.avatarCollapsedLeft : ProfileViewSpec.avatarExpandedLeft,
      width: screenWidth,
      height: collapsed ? ProfileViewSpec.avatarCollapsedSize : ProfileViewSpec.avatarExpandedHeight,
      alignment: collapsed ? Alignment.topLeft : Alignment.center,
      animationDuration: duration,
    );

    final title = ProfileAnimatedText(
      text: '${profile.name} ${profile.surname}',
      style:  getTextTheme(context).titleMedium,
      top: collapsed ? ProfileViewSpec.nameCollapsedTop : ProfileViewSpec.nameExpandedTop,
      left: collapsed ? ProfileViewSpec.nameCollapsedLeft : ProfileViewSpec.nameExpandedLeft,
      width: screenWidth,
      height: ProfileViewSpec.nameHeight,
      alignment: collapsed ? Alignment.topLeft : Alignment.center,
      animationDuration: duration,
    );

    final alias = ProfileText(
      text: profile.alias,
      style: getTextTheme(context).labelLarge,
      top: ProfileViewSpec.aliasTop,
      left: ProfileViewSpec.aliasLeft,
      width: screenWidth,
      height: ProfileViewSpec.aliasHeight,
    );

    final description = ProfileText(
      text: profile.description,
      style:  getTextTheme(context).bodyLarge,
      top: ProfileViewSpec.descriptionTop,
      left: ProfileViewSpec.descriptionLeft,
      width: screenWidth,
      height: ProfileViewSpec.descriptionHeight,
    );

    components
      ..add(avatar)
      ..add(title);

    if (!collapsed) {
      components
        ..add(alias)
        ..add(description);
    }

    return components;
  }
}

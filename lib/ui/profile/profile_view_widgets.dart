import 'package:flutter/material.dart';

class ProfileAnimatedAvatar extends StatelessWidget {
  final String avatarUrl;
  final double radius;
  final double top;
  final double left;
  final double width;
  final double height;
  final Alignment alignment;
  final Duration animationDuration;

  const ProfileAnimatedAvatar({
    required this.avatarUrl,
    required this.radius,
    required this.top,
    required this.left,
    required this.width,
    required this.height,
    required this.alignment,
    required this.animationDuration,
  });

  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      curve: Curves.linear,
      duration: animationDuration,
      top: top,
      left: left,
      width: width,
      height: height,
      child: Column(
        children: [
          AnimatedAlign(
            duration: animationDuration,
            alignment: alignment,
            child: CircleAvatar(
              backgroundImage: NetworkImage(avatarUrl),
              radius: radius,
            ),
          ),
        ],
      ),
    );
  }
}

class ProfileAnimatedText extends StatelessWidget {
  final String text;
  final TextStyle? style;
  final double top;
  final double left;
  final double width;
  final double height;
  final Alignment alignment;
  final Duration animationDuration;

  const ProfileAnimatedText({
    required this.text,
    this.style,
    required this.top,
    required this.left,
    required this.width,
    required this.height,
    required this.alignment,
    required this.animationDuration,
  });

  @override
  Widget build(BuildContext context) {
    return AnimatedPositioned(
      curve: Curves.linear,
      duration: animationDuration,
      top: top,
      left: left,
      width: width,
      height: height,
      child: Column(
        children: [
          AnimatedAlign(
            duration: animationDuration,
            alignment: alignment,
            child: Text(
              text,
              style: style,
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}

class ProfileText extends StatelessWidget {
  final String text;
  final TextStyle? style;
  final double top;
  final double left;
  final double width;
  final double height;

  const ProfileText({
    required this.text,
    this.style,
    required this.top,
    required this.left,
    required this.width,
    required this.height,
  });

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: top,
      left: left,
      width: width,
      height: height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            text,
            style: style,
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}

class ProfileSpacer extends StatelessWidget {
  final double height;
  final double top;
  final double left;

  const ProfileSpacer({
    required this.height,
    required this.top,
    required this.left,
  });

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: top,
      left: left,
      height: height,
      child: SizedBox(height: height),
    );
  }
}

import '../../resources/dimensions.dart';

class ProfileViewSpec {

  ProfileViewSpec._();

  // General
  static const double paddingTop = Dimensions.marginExtraLarge;
  static const double smallGap = 4.0;
  static const double normalGap = 12.0;
  static const double textHeight = 24.0;

  // Avatar
  static const double avatarSmallRadius = 18.0;
  static const double avatarCollapsedTop = 0.0;
  static const double avatarCollapsedLeft = normalGap;
  static const double avatarCollapsedSize = 2 * avatarSmallRadius;

  static const double avatarLargeRadius = 30.0;
  static const double avatarExpandedTop = 12.0;
  static const double avatarExpandedLeft = 0.0;
  static const double avatarExpandedHeight = 2 * avatarLargeRadius;

  // Name
  static const double nameHeight = textHeight;

  static const double nameCollapsedTop = (avatarCollapsedSize - nameHeight) / 2;
  static const double nameCollapsedLeft = avatarCollapsedLeft + avatarCollapsedSize + normalGap;

  static const double nameExpandedTop = avatarExpandedTop + avatarExpandedHeight + normalGap;
  static const double nameExpandedLeft = 0.0;

  // Alias
  static const double aliasHeight = textHeight;

  static const double aliasTop = nameExpandedTop + nameHeight + smallGap;
  static const double aliasLeft = 0.0;

  // Description
  static const double descriptionHeight = textHeight;

  static const double descriptionTop = aliasTop + aliasHeight + normalGap;
  static const double descriptionLeft = 0.0;

  // Profile View
  static const double collapsedHeight = avatarCollapsedSize;
  static const double expandedHeight = descriptionTop + descriptionHeight + smallGap;
}
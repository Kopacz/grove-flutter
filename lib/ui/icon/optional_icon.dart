import 'package:flutter/material.dart';
import 'package:grove_flutter/resources/dimensions.dart';
import 'package:grove_flutter/resources/palette.dart';

class OptionalIcon extends StatelessWidget {
  final bool isVisible;
  final IconData icon;
  final double size;

  const OptionalIcon({
    required this.isVisible,
    required this.icon,
    this.size = Dimensions.iconSizeDefault,
  });

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: isVisible,
      child: Icon(
        icon,
        color: Palette.white,
        size: size,
      ),
    );
  }
}
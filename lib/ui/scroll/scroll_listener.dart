import 'package:flutter/cupertino.dart';

class ScrollListener {
  final void Function()? onScrolledTop;
  final void Function()? onScrolledBottom;
  final void Function()? onScrolledUp;
  final void Function()? onScrolledDown;

  const ScrollListener({
    this.onScrolledTop,
    this.onScrolledBottom,
    this.onScrolledUp,
    this.onScrolledDown,
  });
}

extension ScrollHandling on ScrollController {
  void handleScrollEvents({
    required double previousScroll,
    required double maxScroll,
    required ScrollListener listener,
  }) {
    if (previousScroll > 0 && offset == 0) {
      listener.onScrolledTop?.call();
    } else if (offset == maxScroll) {
      listener.onScrolledBottom?.call();
    } else if (offset < previousScroll) {
      listener.onScrolledUp?.call();
    } else if (offset > previousScroll) {
      listener.onScrolledDown?.call();
    }
  }
}

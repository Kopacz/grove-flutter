import 'package:flutter/material.dart';
import 'package:grove_flutter/ui/text/app_typography.dart';

import '../../resources/dimensions.dart';
import '../../resources/palette.dart';

class HeaderView extends StatelessWidget {
  final String text;

  const HeaderView({required this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Palette.backgroundColor,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: Dimensions.marginNormal,
          vertical: Dimensions.marginSemiLarge,
        ),
        child: Text(
          text,
          style:  getTextTheme(context).titleMedium,
        ),
      ),
    );
  }
}

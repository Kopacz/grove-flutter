import 'package:flutter/cupertino.dart';
import 'package:grove_flutter/resources/palette.dart';

class GreyText extends StatelessWidget {
  final String text;

  const GreyText(this.text);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: Palette.contentColor,
      ),
    );
  }
}

import 'package:grove_flutter/domain/model/clip.dart';
import 'package:grove_flutter/domain/model/like_interaction.dart';
import 'package:grove_flutter/domain/model/relationship.dart';
import 'package:grove_flutter/domain/model/room.dart';

class ProfileData {
  final ProfileMode mode;
  final String name;
  final String surname;
  final String alias;
  final String description;
  final String avatarUrl;
  final List<Room> rooms;
  final List<Clip> clips;
  final List<LikeInteraction> likes;
  final List<Relationship> friends;

  ProfileData({
    required this.mode,
    required this.name,
    required this.surname,
    required this.alias,
    required this.description,
    required this.avatarUrl,
    required this.rooms,
    required this.clips,
    required this.likes,
    required this.friends,
  });
}

enum ProfileMode {
  guest,
  user
}

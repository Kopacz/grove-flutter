class LikeInteraction {
  final String avatar;
  final String alias;
  final String clipName;
  final String date;

  LikeInteraction({
    required this.avatar,
    required this.alias,
    required this.clipName,
    required this.date,
  });
}

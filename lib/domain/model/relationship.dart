class Relationship {
  final String avatar;
  final String name;
  final String alias;
  final RelationshipStatus status;

  Relationship({
    required this.avatar,
    required this.name,
    required this.alias,
    required this.status,
  });
}

enum RelationshipStatus {
  accepted,
  invited,
  declined
}

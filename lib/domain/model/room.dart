class Room {
  final String cover;
  final String title;
  final String members;
  final String likes;
  final String views;
  final bool isMembered;

  Room({
    required this.cover,
    required this.title,
    required this.members,
    required this.likes,
    required this.views,
    required this.isMembered,
  });
}

class Clip {
  final ClipType type;
  final String cover;
  final bool isLiked;

  Clip({
    required this.type,
    required this.cover,
    required this.isLiked,
  });
}


enum ClipType {
  video,
  photo
}

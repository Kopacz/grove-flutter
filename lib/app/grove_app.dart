import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grove_flutter/app/app_cubit.dart';
import 'package:grove_flutter/app/app_router.dart';
import 'package:grove_flutter/app/app_state.dart';
import 'package:grove_flutter/app/app_theme.dart';
import 'package:grove_flutter/data/di/service_locator.dart';
import 'package:grove_flutter/resources/strings.dart';
import 'package:grove_flutter/ui/loader/loader.dart';

class GroveApp extends StatelessWidget {
  const GroveApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AppCubit>(
      create: (_) => serviceLocator.get<AppCubit>()..isOnboardingDisplayed(),
      child: BlocBuilder<AppCubit, AppState>(
        builder: (cubitContext, state) {
          return state.when(
            loading: () => const Loader(),
            success: (onboarded) => _getAppBody(_getStartingRoute(onboarded)),
            error: (_) => _getAppBody(AppRouter.dashboard),
          );
        },
      ),
    );
  }

  String _getStartingRoute(bool isOnboardingDisplayed) {
    if (isOnboardingDisplayed) {
      return AppRouter.dashboard;
    } else {
      return AppRouter.onboarding;
    }
  }

  Widget _getAppBody(String startRoute) {
    return MaterialApp(
      title: Strings.appName,
      initialRoute: startRoute,
      onGenerateRoute: AppRouter.onGenerateRoute,
      theme: AppTheme.general,
    );
  }
}

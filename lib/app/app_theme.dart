import 'package:flutter/material.dart';
import 'package:grove_flutter/resources/dimensions.dart';

import '../resources/palette.dart';

class AppTheme {
  AppTheme._();

  static final general = ThemeData(
    primaryColor: Palette.backgroundColor,
    primaryColorDark: Palette.backgroundDarkColor,
    typography: typography,
    dividerColor: Palette.dividerColor,
    bottomNavigationBarTheme: bottomNavigationBar,
  );

  static final typography = Typography(white: textTheme, black: textTheme);

  static final textTheme = TextTheme(
    displayLarge: TextStyle(
      fontStyle: FontStyle.normal,
      fontSize: 36,
      color: Palette.white,
    ),
    displayMedium: TextStyle(
      fontStyle: FontStyle.normal,
      fontSize: 32,
      color: Palette.white,
    ),
    displaySmall: TextStyle(
      fontStyle: FontStyle.normal,
      fontSize: 28,
      color: Palette.white,
    ),
    headlineLarge: TextStyle(
      fontStyle: FontStyle.italic,
      fontSize: 24,
      color: Palette.white,
    ),
    headlineMedium: TextStyle(
      fontStyle: FontStyle.italic,
      fontSize: 20,
      color: Palette.white,
    ),
    headlineSmall: TextStyle(
      fontStyle: FontStyle.italic,
      fontSize: 16,
      color: Palette.white,
    ),
    titleLarge: TextStyle(
      fontStyle: FontStyle.normal,
      fontSize: 24,
      color: Palette.white,
      fontWeight: FontWeight.bold,
    ),
    titleMedium: TextStyle(
      fontStyle: FontStyle.normal,
      fontSize: 20,
      color: Palette.white,
      fontWeight: FontWeight.bold,
    ),
    titleSmall: TextStyle(
      fontStyle: FontStyle.normal,
      fontSize: 16,
      color: Palette.white,
      fontWeight: FontWeight.bold,
    ),
    bodyLarge: TextStyle(
      fontStyle: FontStyle.normal,
      fontSize: 14,
      color: Palette.white,
    ),
    bodyMedium: TextStyle(
      fontStyle: FontStyle.normal,
      fontSize: 12,
      color: Palette.white,
    ),
    bodySmall: TextStyle(
      fontStyle: FontStyle.normal,
      fontSize: 10,
      color: Palette.white,
    ),
    labelLarge: TextStyle(
      fontStyle: FontStyle.italic,
      fontSize: 14,
      color: Palette.white,
    ),
    labelMedium: TextStyle(
      fontStyle: FontStyle.italic,
      fontSize: 12,
      color: Palette.white,
    ),
    labelSmall: TextStyle(
      fontStyle: FontStyle.normal,
      fontSize: 10,
      color: Palette.white,
    ),
  );

  static final bottomNavigationBar = BottomNavigationBarThemeData(
    backgroundColor: Palette.backgroundColor,
    selectedItemColor: Palette.accentColor,
    selectedIconTheme: IconThemeData(
      color: Palette.accentColor,
      size: Dimensions.iconSizeDefault,
    ),
    selectedLabelStyle: const TextStyle(
      fontSize: Dimensions.fontSizeTextBody,
    ),
    unselectedIconTheme: IconThemeData(
      color: Palette.contentColor,
      size: Dimensions.iconSizeDefault,
    ),
    unselectedItemColor: Palette.contentColor,
    unselectedLabelStyle:
        const TextStyle(fontSize: Dimensions.fontSizeTextBody),
  );
}

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grove_flutter/app/app_state.dart';
import 'package:grove_flutter/data/repository/onboarding_repository.dart';
import 'package:grove_flutter/data/storage/device_storage.dart';

class AppCubit extends Cubit<AppState> {
  final OnboardingRepository repository;

  AppCubit({required this.repository}) : super(const AppState.loading());

  Future<void> isOnboardingDisplayed() async {
    final isOnboardingDisplayed = await repository.isOnboardingDisplayed();

    if (isOnboardingDisplayed ?? false) {
      emit(const AppState.success(isOnboardingDisplayed: true));
    } else {
      emit(const AppState.success(isOnboardingDisplayed: false));
    }
  }
}

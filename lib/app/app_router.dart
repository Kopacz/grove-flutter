import 'package:flutter/material.dart';
import 'package:grove_flutter/feature/authentication/screen/login_screen.dart';
import 'package:grove_flutter/feature/onboarding/screen/onboarding_screen.dart';

import '../feature/dashboard/main/dashboard_screen.dart';

class AppRouter {

  static const onboarding = 'onboarding';
  static const dashboard = 'dashboard';
  static const login = 'login';

  static PageRoute<Object?> onGenerateRoute(RouteSettings settings) {
    final uri = settings.name != null ? Uri.parse(settings.name!) : null;
    final name = uri?.pathSegments[0];
    Widget screen;

    switch (name) {
      case AppRouter.onboarding:
        screen = const OnboardingScreen();
        break;
      case AppRouter.dashboard:
        screen = const DashboardScreen();
        break;
      case AppRouter.login:
        screen = const LoginScreen();
        break;
      default:
        screen = const DashboardScreen();
        break;
    }

    return MaterialPageRoute(builder: (_) => screen, settings: settings);
  }
}
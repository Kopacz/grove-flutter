import 'package:freezed_annotation/freezed_annotation.dart';

part 'app_state.freezed.dart';

@freezed
class AppState with _$AppState {
  const factory AppState.loading() = Loading;

  const factory AppState.success({
    required bool isOnboardingDisplayed,
  }) = Success;

  const factory AppState.error({required String errorMessage}) = Error;
}

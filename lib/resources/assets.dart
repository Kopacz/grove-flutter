class Assets {
  Assets._();

  static const indicatorDots1Vector = 'assets/vectors/indicator_1.svg';
  static const indicatorDots2Vector = 'assets/vectors/indicator_2.svg';
  static const indicatorDots3Vector = 'assets/vectors/indicator_3.svg';

  static const defaultCover = 'assets/images/image_cover.png';
  static const logoTextImage = 'assets/images/image_logo_text.png';
  static const logoTriangleImage = 'assets/images/image_logo_triangle.png';
  static const concertImage = 'assets/images/image_concert.png';
  static const mapImage = 'assets/images/image_map.png';
  static const roomsImage = 'assets/images/image_rooms.png';
  static const clipsImage = 'assets/images/image_clips.png';
  static const friendsImage = 'assets/images/image_friends.png';

  static const onboardingRoomsAnimation = 'assets/animations/lottie_onboarding1.json';
  static const onboardingCommunitiesAnimation = 'assets/animations/lottie_onboarding2.json';
  static const onboardingExploreAnimation = 'assets/animations/lottie_onboarding3.json';
  static const onboardingLocationAnimation = 'assets/animations/lottie_pulse.json';

  static const avatarGuestPlaceholder = "https://via.placeholder.com/600x400?text=Guest";
}

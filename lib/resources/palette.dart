import 'dart:ui';

import 'package:flutter/material.dart';

class Palette {
  Palette._();

  static final backgroundColor = sharkGrey;
  static final backgroundDarkColor = woodsmokeGrey;

  static final accentColor = coniferGreen;

  static final contentColor = manateeGrey;
  static final dividerColor = tunaBlue;

  static final white = const Color(0xFFFFFFFF);
  static final black = const Color(0xFF000000);

  static final sharkGrey = const Color(0xFF1D1F27);
  static final woodsmokeGrey = const Color(0xFF13151B);
  static final manateeGrey = const Color(0xFF9094A7);

  static final coniferGreen = const Color(0xFFB4D64A);
  static final fuegoGreen = const Color(0xFFBEDB12);

  static final tunaBlue = const Color(0xFF323641);
  static final javaBlue = const Color(0xFF24bdca);
}

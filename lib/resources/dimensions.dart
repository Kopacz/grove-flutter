class Dimensions {
  Dimensions._();

  static const marginExtraSmall = 4.0;
  static const marginSmall = 8.0;
  static const marginMedium = 12.0;
  static const marginNormal = 16.0;
  static const marginSemiLarge = 20.0;
  static const marginLarge = 24.0;
  static const marginExtraLarge = 32.0;
  static const marginBottomSheetBottom = 64.0;

  static const fontSizeTextBody = 12.0;

  static const borderSmall = 1.0;
  static const borderDefault = 2.0;

  static const iconSizeSmall = 12.0;
  static const iconSizeSemiMedium = 16.0;
  static const iconSizeMedium = 20.0;
  static const iconSizeDefault = 24.0;
  static const iconSizeLarge= 40.0;

  static const roundedCornerSmall = 6.0;
  static const roundedCornerNormal = 18.0;

  static const dividerSizeDefault = 1.0;

  static const avatarRadiusSmall = 12.0;
  static const avatarRadiusDefault = 24.0;

  static const indicatorSizeDefault = 4.0;
}

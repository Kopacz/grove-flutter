class Strings {
  Strings._();

  // App
  static const appName = 'Grove Flutter';

  // General
  static const loadingLabel = 'Loading...';
  static const loginLabel = 'Login';
  static const logoutLabel = 'Logout';
  static const nextLabel = 'Next';
  static const skipLabel = 'Skip';
  static const backLabel = 'Back';

  // Onboarding
  static const onboardingPage1Title = 'Discover Rooms';
  static const onboardingPage1Description = 'Rooms on Grove are where you create and share experiences. Members contribute photos and videos based on topic, location, groups, etc.';

  static const onboardingPage2Title = 'Connect Communities';
  static const onboardingPage2Description = 'Get all perspectives! Create or join a room to post content and connect with friends, teams, family and more.';

  static const onboardingPage3Title = 'Explore Grove';
  static const onboardingPage3Description = 'Grove uses location to enjoy content in a new way! Allow location permissions to find rooms near you.';

  static const onboardingLocationTitle = 'Explore the Map';
  static const onboardingLocationDescription = 'Allow Grove to use location settings to discover rooms near you and improve your overall experience!';
  static const onboardingLocationButtonText = 'Enable Location Setting';

  // Dashboard
  static const dashboardHomeItemLabel = 'Home';
  static const dashboardExploreItemLabel = 'Explore';
  static const dashboardPostItemLabel = 'Post';
  static const dashboardActivityItemLabel = 'Activity';
  static const dashboardProfileItemLabel = 'Me';

  // Authentication
  static const authenticationLoginTitle = 'Login to your\n account below';
  static const authenticationLoginDescription = 'Share content with your friends, teams and communities by creating an account below:';
  static const authenticationLoginButtonText = 'Sign in with Google';

  // Profile
  static const profileRoomsTabItemLabel = 'Rooms';
  static const profileRoomsTabLabel = 'My Rooms';
  static const profileClipsTabItemLabel = 'Clips';
  static const profileClipsTabLabel = 'My Clips';
  static const profileLikesTabItemLabel = 'Likes';
  static const profileLikesTabLabel = 'My Likes';
  static const profileFriendsTabItemLabel = 'Friends';
  static const profileFriendsTabLabel = 'My Friends';

  static const profileRoomsInitialTitleText = 'Find Rooms To Join';
  static const profileRoomsInitialDescriptionText = 'Add your photos and videos to your favorite rooms. Help grow the rooms you love.';
  static const profileRoomsInitialButtonText = 'Explore Rooms';

  static const profileClipsInitialTitleText = 'Post Clips';
  static const profileClipsInitialDescriptionText = 'Explore the Grove map or feed to discover rooms that interest you.';
  static const profileClipsInitialButtonText = 'Post a Clip';

  static const profileLikesInitialTitleText = 'Like Clips';
  static const profileLikesInitialDescriptionText = 'Show your love! Simply click on the heart to ‘like’ any photo/video and share your support.';
  static const profileLikesInitialButtonText = 'Find Clips to Like';
  static const profileLikesInteractionText = ' liked your clip:';

  static const profileFriendsInitialTitleText = 'Invite or Find Friends';
  static const profileFriendsInitialDescriptionText = 'Grove is more fun with friends. Instantly share your clips in one, easy place!';
  static const profileFriendsInitialButtonText = 'Invite Friends';
}